import numpy as np
import lalsimulation as lalsim
import lal 

class Binary:
    
    def __init__(self,
        m1=10.,m2=10.,
        alpha=50.,beta=30.,gamma=0.,
        frame_rate = 30.,omega = 1.
    ):
        """
        A class for representing binaries.

        Parameters
        ----------
        m1,m2: floats
            masses of 1st and 2nd star, repsectively
        inclination: float
            inclination angle of binary wrt viewer
            in degrees, between [0,90] deg.
        azimuth: float
            azimuthal angle of viewer in coordinates
            of the binary, between [0,360] deg.
        phase: float
            phase of the binary, between [0,360] deg.
        frame_rate: float
            frames per second
        omega: float
            rotations per second
        """
        self.m1 = m1
        self.m2 = m2
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self._omega = omega
        self.frame_rate = frame_rate
        self.precession='off'
        self._phase1 = self.gamma_rad
        self._D = np.identity(3)
        self._C = np.identity(3)
        self._B = np.identity(3)

        self._pos1_projected = []
        self._pos2_projected = []
        self._pos_spin1_projected = []
        self._pos_spin2_projected = []
        self._r = 1.
        

    @property
    def r(self):
        return self._r
    
    @property
    def omega(self):
        return self._omega

    @property
    def alpha_rad(self):
        return self.alpha*np.pi/180.

    @property
    def beta_rad(self):
        return self.beta*np.pi/180.

    @property
    def gamma_rad(self):
        return self.gamma*np.pi/180.

    @property
    def phase1(self):
        return self._phase1
    
    @phase1.setter
    def phase1(self,phase_rad):
        self._phase1 = phase_rad % 2*np.pi 
    
    @property
    def phase2(self):
        return self.phase1+np.pi
        
    def orbit(self):
        self._phase1 = self.phase1 + 2.*np.pi*self.omega/self.frame_rate

    @property
    def Dmatrix(self):
        self._D[0,0] = self._D[1,1] = np.cos(self.alpha_rad)
        self._D[1,0] = -np.sin(self.alpha_rad)
        self._D[0,1] = np.sin(self.alpha_rad)
        return self._D

    @property
    def Cmatrix(self):
        self._C[1,1] = self._C[2,2] = np.cos(self.beta_rad)
        self._C[2,1] = -np.sin(self.beta_rad)
        self._C[1,2] = np.sin(self.beta_rad)
        return self._C

    @property
    def Bmatrix(self):
        self._B[0,0] = self._B[1,1] = np.cos(self.gamma_rad)
        self._B[1,0] = -np.sin(self.gamma_rad)
        self._B[0,1] = np.sin(self.gamma_rad)
        return self._B
    
    @property
    def Amatrix(self):
       return np.matmul(np.matmul(self.Bmatrix,self.Cmatrix),self.Dmatrix)

    @property
    def pos1(self):
        """
        position of primary in XYZ coordinates of binary plane 
        """
        return (self.r*(self.m2/(self.m1+self.m2))
                * np.array([np.cos(self.phase1),np.sin(self.phase1),0])
        )

    @property
    def pos2(self):
        return (self.r*(self.m1/(self.m1+self.m2))
                * np.array([np.cos(self.phase2),np.sin(self.phase2),0])
        )
    
    def project(self):
        return np.matmul(self.Amatrix,self.pos1),np.matmul(self.Amatrix,self.pos2)

    def evolve(self,n_steps=1):
        for i in range(n_steps):
            self.orbit()
            pos1_proj,pos2_proj = self.project()
            self._pos1_projected.append(pos1_proj) 
            self._pos2_projected.append(pos2_proj)

    @property
    def pos1_projected(self):
        return np.array(self._pos1_projected)

    @property
    def pos2_projected(self):
        return np.array(self._pos2_projected) 
    
    @property
    def pos_spin1_projected(self):
        return np.array(self._pos_spin1_projected)

    @property
    def pos_spin2_projected(self):
        return np.array(self._pos_spin2_projected)

class InspiralingBinary(Binary):

    def __init__(self,
        m1=10.,m2=10.,
        alpha=50.,beta=30.,gamma=0.,
        frame_rate = 30.,omega0 = 1.
    ):

        super().__init__(
            m1,m2,
            alpha,beta,gamma,
            frame_rate,omega0
        )
        
        self.Mc = ((self.m1*self.m2)**(3./5))/((self.m1+self.m2)**(1./5))
        self.G = 4.3e-3 * 3.1e13  # km Msol^-1 (km/s)^2
        self.c = 3e5 # km/s

        self.tc = (((2.*omega0)**(-8./3))
                    * (5./((8.*np.pi)**(8./3)))
                    * (((self.c**3.)/(self.G*self.Mc))**(5./3))
        )
        print('tc',self.tc)        

        self.rmax = 2.*(self.G/(self.c**2))*(self.m1+self.m2)
        self.omegamax = np.sqrt(self.G*(self.m1+self.m2)/self.rmax**3)
        self.frame_rate = self.omegamax        
 
        self.t = [0.]
        pos1_proj,pos2_proj = self.project()
        self._pos1_projected = [pos1_proj]
        self._pos2_projected = [pos2_proj]         

    @property
    def nframes(self):
        return len(self.t)

    @property
    def omega(self):
        return (0.5)*((((8.*np.pi)**(8./3))/5.)
                        * (self.G*self.Mc/(self.c**3.))**(5./3)
                        * (self.tc - self.t[-1])
                  )**(-3./8)

    @property
    def r(self):
        return (self.G*(self.m1+self.m2)/(self.omega**2))**(1./3)

    def inspiral(self):
        while self.r > self.rmax:
            print('r,rmax:',self.r,self.rmax)
            self.t.append(self.t[-1]+(1./self.frame_rate))
            self.orbit()
            if isinstance(self.r,complex):
                print('r going complex, breaking out of loop')
                break
            pos1_proj,pos2_proj = self.project()
            self._pos1_projected.append(pos1_proj)
            self._pos2_projected.append(pos2_proj)     


class Binary_with_spins(Binary):
# Initialize new class for binary with spins
    def __init__(self,
        m1=10.,m2=10.,
        alpha=50.,beta=30.,gamma=0.,
        frame_rate = 30.,omega0 = 1.,
        chi1 = 0.95, chi2 = 0.95,
        tilt1 = np.pi/3, tilt2 = -np.pi/8, phi12 = np.pi,
        dt=0.1
    ):

        super().__init__(
            m1,m2,
            alpha,beta,gamma,
            frame_rate,omega0
        )
        chi1_vec = [chi1*np.sin(tilt1), 0.0, chi1*np.cos(tilt1)]
        chi2_vec = [chi2*np.sin(tilt2)*np.cos(phi12), chi2*np.sin(tilt2)*np.sin(phi12), chi2*np.cos(tilt2)]

        self.chi1 = chi1_vec
        self.chi2 = chi2_vec
        self.dt = dt
        self.omega0 = omega0
        self.omega_arr = [self.omega0]
        self.chi1_arr = [chi1_vec]
        self.chi2_arr = [chi2_vec]
        self.Mc = ((self.m1*self.m2)**(3./5))/((self.m1+self.m2)**(1./5))
        self.G = 4.3e-3 * 3.1e13  # km Msol^-1 (km/s)^2
        self.c = 3e5 # km/s
      

        self.tc = (((2.*omega0)**(-8./3))
                    * (5./((8.*np.pi)**(8./3)))
                    * (((self.c**3.)/(self.G*self.Mc))**(5./3))
        )
        print('tc',self.tc)        

        self.rmax = 2.*(self.G/(self.c**2))*(self.m1+self.m2)
        self.omegamax = np.sqrt(self.G*(self.m1+self.m2)/self.rmax**3)
        self.frame_rate = self.omegamax 
       
 
        self.t = [0.]
        pos1_proj,pos2_proj = self.project()
        self._pos1_projected = [pos1_proj]
        self._pos2_projected = [pos2_proj]     

        pos_spin1_proj,pos_spin2_proj = self.project_spins()
        self._pos_spin1_projected = [pos_spin1_proj]
        self._pos_spin2_projected = [pos_spin2_proj] 

    def evolve(self):    

        m1_SI = self.m1 * lal.MSUN_SI
        m2_SI = self.m2 * lal.MSUN_SI

        MT_s = (m1_SI + m2_SI) * (lal.G_SI/lal.C_SI**3)
        f_ISCO = 6.**(-1.5) / (np.pi * MT_s) - 0.05


        chi1x_start, chi1y_start, chi1z_start = self.chi1
        chi2x_start, chi2y_start, chi2z_start = self.chi2


        Lnx_start = 0.0
        Lny_start = 0.0
        Lnz_start = 1.0

        E1x_start = 1.0
        E1y_start = 0.0
        E1z_start = 0.0

        lalpars=lal.CreateDict()
        lalsim.SimInspiralWaveformParamsInsertPNSpinOrder(lalpars, 6)
        lalsim.SimInspiralWaveformParamsInsertPNPhaseOrder(lalpars, 7)
        lalsim.SimInspiralWaveformParamsInsertLscorr(lalpars, 1)
        lalsim.SimInspiralWaveformParamsInsertFinalFreq(lalpars, self.omegamax)
        print(f_ISCO, self.omegamax)

        approx = lalsim.GetApproximantFromString("SpinTaylorT5")

        dictparams = {'phiRef': 0.0, 'deltaT': self.dt, 'm1_SI': m1_SI, 'm2_SI': m2_SI, 'fStart': self.omega0, 'fRef': 20.,
        's1x': chi1x_start, 's1y': chi1y_start, 's1z': chi1z_start,
        's2x': chi2x_start, 's2y': chi2y_start, 's2z': chi2z_start, 'lnhatx': Lnx_start, 'lnhaty': Lny_start,
        'lnhatz': Lnz_start, 'e1x': E1x_start, 'e1y': E1y_start, 'e1z': E1z_start, 'LALparams': lalpars, 'approx': approx}
        
        V, phase, c1x, c1y, c1z, c2x, c2y, c2z, Lnx, Lny, Lnz, E1x, E1y, E1z = lalsim.SimInspiralSpinTaylorOrbitalDriver(**dictparams)
        omega_arr = np.array((V.data.data**3/(np.pi)/MT_s))
        self.tc = self.dt * len(self.omega_arr)
        chi1_arr = np.array([c1x.data.data, c1y.data.data, c1z.data.data]).T
        chi2_arr = np.array([c2x.data.data, c2y.data.data, c2z.data.data]).T

        return(omega_arr, chi1_arr, chi2_arr)


    @property
    def omega(self):
        return(self.omega_arr[-1])



    @property
    def pos_spin1(self):
        """
        position of primary spin arrow base in XYZ coordinates of binary plane == position of primary
        """
        s1_arr = np.array(self.chi1_arr[-1])
        return (1.5*s1_arr/np.linalg.norm(s1_arr)
        )
    

    @property
    def pos_spin2(self):
        s2_arr = np.array(self.chi2_arr[-1])
        return (1.5*s2_arr/np.linalg.norm(s2_arr)
        )    
    

    def project_spins(self):
        return np.matmul(self.Amatrix,self.pos_spin1),np.matmul(self.Amatrix,self.pos_spin2)
       

    @property
    def nframes(self):
        return len(self.t)


    @property
    def r(self):
        # # Compute binary separation (up to 1.5 PN order) given an orbital angular frequency, omega
        # q = min(self.m1, self.m2) / max(self.m1, self.m2)
        # c1 = np.linalg.norm(self.chi1)
        # c2 = np.linalg.norm(self.chi2)
        # #tildeomega = (self.m1+self.m2) * self.omega * (2*np.pi*lal.MSUN_SI*lal.G_SI)
        # tildeomega = self.G*(self.m1+self.m2)/self.omega

        # #rsep =  tildeomega**(-2/3) #* (1 - tildeomega**(2/3) * ( 1- q/(3*(1+q)**2) ) 
        # #- tildeomega / (3*(1+q)**2) * ( (2+3*q)*c1*np.cos(self.tilt1) + q*(3+2*q)*c2*np.cos(self.tilt2) )
        # #+ tildeomega**(4/3) * q/(2*(1+q)**2) * (19/2 +  (2*q)/(9*(1+q)**2)+ c1*c2*(2*np.cos(self.tilt1)*np.cos(self.tilt2) - 
        # #                                                                           np.cos(self.phi12)*np.sin(self.tilt1)*np.sin(self.tilt2)) ) 
        # #)

        rsep = (self.G*(self.m1+self.m2)/(self.omega**2))**(1./3)
        #print(rsep)
        return rsep
        
    


    def inspiral(self):
       omega_arr, chi1_arr, chi2_arr = self.evolve()
       print(self.dt*len(omega_arr))
       for i in range(len(omega_arr)):
           self.omega_arr[-1] = omega_arr[i]
           self.chi1_arr[-1] = chi1_arr[i]
           self.chi2_arr[-1] = chi2_arr[i]
           self.t.append(self.t[-1]+(1./self.frame_rate))
           self.orbit()
           pos1_proj,pos2_proj = self.project()
           self._pos1_projected.append(pos1_proj)
           self._pos2_projected.append(pos2_proj)
           pos_spin1_proj,pos_spin2_proj = self.project_spins()
           self._pos_spin1_projected.append(pos_spin1_proj)
           self._pos_spin2_projected.append(pos_spin2_proj)



class Spin_Projection(Binary):
# Initialize new class for binary with spins
    def __init__(self,
        m1=10.,m2=10.,
        projection='XY',
        frame_rate = 30.,omega0 = 1.,
        chi1 = 0.95, chi2 = 0.95,
        tilt1 = np.pi/3, tilt2 = -np.pi/8, phi12 = np.pi,
        dt=0.1
    ):
        
        if projection=='XY': 
            alpha, beta, gamma = 180., 90., 90.
        elif projection=='XZ': # Horizontal motion with Z pointing upward
            alpha, beta, gamma = 90., 0., 180.
        elif projection=='ZY': # Vertical motion with Z pointing leftward
            alpha, beta, gamma = 90., 90., 0.

        super().__init__(
            m1,m2,
            alpha,beta,gamma,
            frame_rate,omega0
        )
        chi1_vec = [chi1*np.sin(tilt1), 0.0, chi1*np.cos(tilt1)]
        chi2_vec = [chi2*np.sin(tilt2)*np.cos(phi12), chi2*np.sin(tilt2)*np.sin(phi12), chi2*np.cos(tilt2)]

        self.chi1 = chi1_vec
        self.chi2 = chi2_vec
        self.dt = dt
        self.omega0 = omega0
        self.omega_arr = [self.omega0]
        self.chi1_arr = [chi1_vec]
        self.chi2_arr = [chi2_vec]
        self.Mc = ((self.m1*self.m2)**(3./5))/((self.m1+self.m2)**(1./5))
        self.G = 4.3e-3 * 3.1e13  # km Msol^-1 (km/s)^2
        self.c = 3e5 # km/s
        self._phase1 = np.pi
      

        self.tc = (((2.*omega0)**(-8./3))
                    * (5./((8.*np.pi)**(8./3)))
                    * (((self.c**3.)/(self.G*self.Mc))**(5./3))
        )
        print('tc',self.tc)        

        self.rmax = 2.*(self.G/(self.c**2))*(self.m1+self.m2)
        self.omegamax = np.sqrt(self.G*(self.m1+self.m2)/self.rmax**3)
        self.frame_rate = self.omegamax 
       
 
        self.t = [0.]
        pos1_proj0,pos2_proj0 = self.project()
        self._pos1_projected = [pos1_proj0]
        self._pos2_projected = [pos2_proj0]
        self._phi12 = [phi12]    

        pos_spin1_proj,pos_spin2_proj = self.project_spins()
        self._pos_spin1_projected = [pos_spin1_proj]
        self._pos_spin2_projected = [pos_spin2_proj] 

    def evolve(self):    

        m1_SI = self.m1 * lal.MSUN_SI
        m2_SI = self.m2 * lal.MSUN_SI

        MT_s = (m1_SI + m2_SI) * (lal.G_SI/lal.C_SI**3)
        f_ISCO = 6.**(-1.5) / (np.pi * MT_s) - 0.05

        chi1x_start, chi1y_start, chi1z_start = self.chi1
        chi2x_start, chi2y_start, chi2z_start = self.chi2


        Lnx_start = 0.0
        Lny_start = 0.0
        Lnz_start = 1.0

        E1x_start = 1.0
        E1y_start = 0.0
        E1z_start = 0.0

        lalpars=lal.CreateDict()
        lalsim.SimInspiralWaveformParamsInsertPNSpinOrder(lalpars, 6)
        lalsim.SimInspiralWaveformParamsInsertPNPhaseOrder(lalpars, 7)
        lalsim.SimInspiralWaveformParamsInsertLscorr(lalpars, 1)
        lalsim.SimInspiralWaveformParamsInsertFinalFreq(lalpars, self.omegamax)

        approx = lalsim.GetApproximantFromString("SpinTaylorT5")

        dictparams = {'phiRef': 0.0, 'deltaT': self.dt, 'm1_SI': m1_SI, 'm2_SI': m2_SI, 'fStart': self.omega0, 'fRef': 20.,
        's1x': chi1x_start, 's1y': chi1y_start, 's1z': chi1z_start,
        's2x': chi2x_start, 's2y': chi2y_start, 's2z': chi2z_start, 'lnhatx': Lnx_start, 'lnhaty': Lny_start,
        'lnhatz': Lnz_start, 'e1x': E1x_start, 'e1y': E1y_start, 'e1z': E1z_start, 'LALparams': lalpars, 'approx': approx}
        
        V, phase, c1x, c1y, c1z, c2x, c2y, c2z, Lnx, Lny, Lnz, E1x, E1y, E1z = lalsim.SimInspiralSpinTaylorOrbitalDriver(**dictparams)
        omega_arr = np.array((V.data.data**3/(np.pi)/MT_s))
        self.tc = self.dt * len(self.omega_arr)
        chi1_arr = np.array([c1x.data.data, c1y.data.data, c1z.data.data]).T
        chi2_arr = np.array([c2x.data.data, c2y.data.data, c2z.data.data]).T

        chi1inplane_out = np.array([c1x.data.data, c1y.data.data]).T
        chi2inplane_out = np.array([c2x.data.data, c2y.data.data]).T

        phi12_arr = []
        for i in range(len(chi1inplane_out)):
            cos_phi12 = np.clip(np.dot(chi1inplane_out[i], chi2inplane_out[i]) / (
            np.linalg.norm(chi1inplane_out[i]) * np.linalg.norm(chi2inplane_out[i])), -1., 1.)
            phi12 = np.arccos(cos_phi12)
            if np.sign(np.dot(np.array([0., 0., 1.]),
                   np.cross(chi1_arr[i], chi2_arr[i]))) < 0:
                phi12 = 2. * np.pi - phi12
            phi12_arr.append(phi12)
        phi12_arr = np.array(phi12_arr)

        return(omega_arr, chi1_arr, chi2_arr, phi12_arr)


    @property
    def omega(self):
        return(self.omega_arr[-1])



    @property
    def pos_spin1(self):
        """
        position of primary spin arrow base in XYZ coordinates of binary plane == position of primary
        """
        s1_arr = np.array(self.chi1_arr[-1])
        return (1.5*s1_arr/np.linalg.norm(s1_arr)
        )
    

    @property
    def pos_spin2(self):
        s2_arr = np.array(self.chi2_arr[-1])
        return (1.5*s2_arr/np.linalg.norm(s2_arr)
        )  
    

    def project_spins(self):
        return np.matmul(self.Amatrix,self.pos_spin1),np.matmul(self.Amatrix,self.pos_spin2)
       

    @property
    def nframes(self):
        return len(self.t)


    @property
    def r(self):
        rsep = 2500.
        print(rsep)
        return rsep
        
    


    def inspiral(self):
       omega_arr, chi1_arr, chi2_arr, phi12_arr = self.evolve()
       print(self.dt*len(omega_arr))
       pos1_proj0,pos2_proj0 = self.project()
       pos1_projected = [pos1_proj0]*len(omega_arr)
       pos2_projected = [pos2_proj0]*len(omega_arr)

       for i in range(len(omega_arr)):
           self.omega_arr[-1] = omega_arr[i]
           self.chi1_arr[-1] = chi1_arr[i]
           self.chi2_arr[-1] = chi2_arr[i]
           self.t.append(self.t[-1]+(1./self.frame_rate))
           self.orbit()
           #pos1_proj,pos2_proj = [1.,0.,0.], [-1.,0.,0.]
           self._pos1_projected.append(pos1_projected[i])
           self._pos2_projected.append(pos2_projected[i])
           pos_spin1_proj,pos_spin2_proj = self.project_spins()
           self._pos_spin1_projected.append(pos_spin1_proj)
           self._pos_spin2_projected.append(pos_spin2_proj)
           self._phi12.append(phi12_arr[i])


