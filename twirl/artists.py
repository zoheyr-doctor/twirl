import numpy as np
import twirl.binary
from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse,Circle,FancyArrow,PathPatch
from mpl_toolkits.mplot3d import Axes3D, proj3d


class BinaryBlackHole:

    """
    A class to define the matplotlib artists
    for a binary black hole.
    """

    def __init__(self,
        binary,
        ax,
        name,
        BH_scale = 1,
    ):
        """
        Parameters
        ----------
        binary: Binary
            An evolved Binary object
        ax: axis object
            The matplotlib axis to plot the artists on
        name: str
            unique name of the binary for identification
        BH_scale: float
            scale of BH on image WRT BH mass in solar masses

        Returns
        -------
        None
        """
        self.binary = binary
        self.ax = ax
        self.name = name
        self.BH_scale = BH_scale
        self.edgecolor='white',
        self.facecolor='black',
        self.artists = {}

    def setup_artists(self):
        z1,x1,y1 = self.binary.pos1_projected[0,:]
        z2,x2,y2 = self.binary.pos2_projected[0,:]
        self.artists['{}_m1'.format(self.name)] = self.ax.add_artist(
            Circle((x1,y1), radius=self.BH_scale*self.binary.m1,
                edgecolor='white',
                facecolor='black'
            )
        )
        self.artists['{}_m2'.format(self.name)] = self.ax.add_artist(
            Circle((x2,y2), radius=self.BH_scale*self.binary.m2,
                edgecolor='white',
                facecolor='black'
            )
        )
        # make sure star that is closer is shown on top of the other
        if z1 > z2:
            zorder_1, zorder_2 = 2, 1
        else:
            zorder_1, zorder_2 = 1, 2
        self.artists['{}_m1'.format(self.name)].set_zorder(zorder_1)
        self.artists['{}_m2'.format(self.name)].set_zorder(zorder_2)

        return self.artists

    def get_artists(self,i):
        z1,x1,y1 = self.binary.pos1_projected[i,:]
        z2,x2,y2 = self.binary.pos2_projected[i,:]
        self.artists['{}_m1'.format(self.name)].set_center((x1,y1))
        self.artists['{}_m2'.format(self.name)].set_center((x2,y2)) 

        # make sure star that is closer is shown on top of the other
        if z1 > z2:
            zorder_1, zorder_2 = 2, 1
        else:
            zorder_1, zorder_2 = 1, 2
        self.artists['{}_m1'.format(self.name)].set_zorder(zorder_1)
        self.artists['{}_m2'.format(self.name)].set_zorder(zorder_2)

        return self.artists
    

class BBH_with_spins:

    """
    A class to define the matplotlib artists
    for a binary black hole.
    """

    def __init__(self,
        binary,
        ax,
        name,
        extent=5000,
        BH_scale = 1.,
        show_axes = False
    ):
        """
        Parameters
        ----------
        binary: Binary
            An evolved Binary object
        ax: axis object
            The matplotlib axis to plot the artists on
        name: str
            unique name of the binary for identification
        BH_scale: float
            scale of BH on image WRT BH mass in solar masses

        Returns
        -------
        None
        """
        self.binary = binary
        self.ax = ax
        self.name = name
        self.BH_scale = BH_scale
        self.edgecolor='white',
        self.facecolor='black',
        self.show_axes = show_axes
        self.extent = extent
        self.x0, self.y0, = -2500,2500
        self.artists = {}

    def setup_artists(self):
        z1,x1,y1 = self.binary.pos1_projected[0,:]
        z2,x2,y2 = self.binary.pos2_projected[0,:]

        s1z, s1x, s1y = 300.*self.BH_scale*self.binary.pos_spin1_projected[0,:]
        s2z, s2x, s2y = 300.*self.BH_scale*self.binary.pos_spin2_projected[0,:]

        arrow_color = 'k'

        arrow_prop_dict = dict(width=1., color=arrow_color, head_width=10.*self.BH_scale, head_length=20.*self.BH_scale)

        self.artists['{}_m1'.format(self.name)] = self.ax.add_artist(
            Circle((x1,y1), radius=self.BH_scale*self.binary.m1,
                edgecolor='white',
                facecolor='black'
            )
        )
        self.artists['{}_s1'.format(self.name)] = self.ax.add_artist(
            FancyArrow(x1, y1, (s1x-x1), (s1y-y1), **arrow_prop_dict)
            )
        self.artists['{}_m2'.format(self.name)] = self.ax.add_artist(
            Circle((x2,y2), radius=self.BH_scale*self.binary.m2,
                edgecolor='white',
                facecolor='black'
            )
        )
        self.artists['{}_s2'.format(self.name)] = self.ax.add_artist(
            FancyArrow(x2, y2, (s2x-x2), (s2y-y2), **arrow_prop_dict)
        )
        # make sure star that is closer is shown on top of the other, and the spin vector is on top if the spin "points out"
        if z1 > z2:
            zorder_1, zorder_2 = 5, 2
        else:
            zorder_1, zorder_2 = 2, 5
        
        if s1z > z1:
            zorder_s1 = zorder_1 + 1
        else: 
            zorder_s1 = zorder_1 - 1

        if s2z > z2:
            zorder_s2 = zorder_2 + 1
        else:
            zorder_s2 = zorder_2 - 1
        self.artists['{}_m1'.format(self.name)].set_zorder(zorder_1)
        self.artists['{}_m2'.format(self.name)].set_zorder(zorder_2)
        self.artists['{}_s1'.format(self.name)].set_zorder(zorder_s1)
        self.artists['{}_s2'.format(self.name)].set_zorder(zorder_s2)

        if self.show_axes:
            # Draw cartesian axes for reference:

            _, dx, dy = np.matmul(self.binary.Amatrix,np.array([600.,0.,0.]))
            self.artists['{}_xaxis'.format(self.name)] = self.ax.add_artist(
                FancyArrow(self.x0, self.y0, dx, dy, **arrow_prop_dict)
            )
            

            _, dx, dy = np.matmul(self.binary.Amatrix,np.array([0.,600.,0.]))
            self.artists['{}_yaxis'.format(self.name)] = self.ax.add_artist(
                FancyArrow(self.x0, self.y0, dx, dy, **arrow_prop_dict)
            )

            _, dx, dy = np.matmul(self.binary.Amatrix,np.array([0.,0.,600.]))
            self.artists['{}_zaxis'.format(self.name)] = self.ax.add_artist(
                FancyArrow(self.x0, self.y0, dx, dy, **arrow_prop_dict)
            )
            self.artists['{}_zlabel'.format(self.name)] = self.ax.text((self.x0+dx*1.5), (self.y0+dy*1.5), "Z", color=arrow_color, fontsize=25)



        return self.artists

    def get_artists(self,i):
        z1,x1,y1 = self.binary.pos1_projected[i,:]
        z2,x2,y2 = self.binary.pos2_projected[i,:]
        s1z, s1x, s1y = 300.*self.binary.pos_spin1_projected[i,:]
        s2z, s2x, s2y = 300.*self.binary.pos_spin2_projected[i,:]
        self.artists['{}_m1'.format(self.name)].set_center((x1,y1))
        self.artists['{}_m2'.format(self.name)].set_center((x2,y2))
        arrow_prop_dict = dict(width=1., head_width=10.*self.BH_scale, head_length=20.*self.BH_scale)
        self.artists['{}_s1'.format(self.name)].set_data(x=x1, y=y1, dx=(s1x), dy=(s1y), **arrow_prop_dict)
        self.artists['{}_s2'.format(self.name)].set_data(x=x2, y=y2, dx=(s2x), dy=(s2y), **arrow_prop_dict) 

        # make sure star that is closer is shown on top of the other, and the spin vector is on top if the spin "points out"
        if z1 > z2:
            zorder_1, zorder_2 = 5, 2
        elif z2 > z1:
            zorder_1, zorder_2 = 2, 5
        
        if s1z >= z1:
            zorder_s1 = zorder_1 + 1
        elif s1z < z1: 
            zorder_s1 = zorder_1 - 1

        if s2z >= z2:
            zorder_s2 = zorder_2 + 1
        elif s2z < z2:
            zorder_s2 = zorder_2 - 1
        self.artists['{}_m1'.format(self.name)].set_zorder(zorder_1)
        self.artists['{}_m2'.format(self.name)].set_zorder(zorder_2)
        self.artists['{}_s1'.format(self.name)].set_zorder(zorder_s1)
        self.artists['{}_s2'.format(self.name)].set_zorder(zorder_s2)

        return self.artists
    


class BlackHoleRingdown:
    def __init__(self,
        M,
        amplitude0,
        omega,
        tau,
        ax,
        name,
        BH_scale = 1
    ):
        self.M = M
        self.tau = tau
        self._amplitude0 = amplitude0
        self.omega = omega
        self.ax = ax
        self.name = name
        self.artists = {}
        self.BH_scale = BH_scale

    def amplitude(self,i):
        return self._amplitude0 * np.exp(-i/self.tau) * np.cos(2.*2.*np.pi*i/self.tau)

    def get_height_width(self,i):
        return 1 + self.amplitude(i), 1 - self.amplitude(i) 

    def setup_artists(self):
        height,width = self.get_height_width(0)
        self.artists['{}_ringdown'.format(self.name)] = self.ax.add_artist(
            Ellipse((0,0),
                self.BH_scale*self.M*height,self.BH_scale*self.M*width,
                edgecolor='white',facecolor='black'
            )
        )
        return self.artists

    def get_artists(self,i):
        height,width = self.get_height_width(i)
        self.artists['{}_ringdown'.format(self.name)].height = self.BH_scale*self.M*height
        self.artists['{}_ringdown'.format(self.name)].width = self.BH_scale*self.M*width
        return self.artists       
         


class BBH_spin_projection:

    """
    A class to define the matplotlib artists
    for a binary black hole.
    """

    def __init__(self,
        binary,
        ax,
        name,
        extent=5000,
        BH_scale = 1.,
        show_phi12 = False
    ):
        """
        Parameters
        ----------
        binary: Binary
            An evolved Binary object
        ax: axis object
            The matplotlib axis to plot the artists on
        name: str
            unique name of the binary for identification
        BH_scale: float
            scale of BH on image WRT BH mass in solar masses

        Returns
        -------
        None
        """
        self.binary = binary
        self.ax = ax
        self.name = name
        self.BH_scale = BH_scale
        self.edgecolor='white',
        self.facecolor='black',
        self.show_phi12 = show_phi12
        self.extent = extent
        self.x0, self.y0, = -2500,2500
        self.x00, self.y00, = 0,0
        self.artists = {}

    def setup_artists(self):
        z1,x1,y1 = self.binary.pos1_projected[0,:]
        z2,x2,y2 = self.binary.pos2_projected[0,:]

        s1z, s1x, s1y = 200.*self.BH_scale*self.binary.pos_spin1_projected[0,:]
        s2z, s2x, s2y = 200.*self.BH_scale*self.binary.pos_spin2_projected[0,:]

        arrow_color = 'k'

        arrow_prop_dict = dict(width=1., color=arrow_color, head_width=10.*self.BH_scale, head_length=20.*self.BH_scale)

        self.artists['{}_m1'.format(self.name)] = self.ax.add_artist(
            Circle((x1,y1), radius=self.BH_scale*self.binary.m1,
                edgecolor='white',
                facecolor='black'
            )
        )
        self.artists['{}_s1'.format(self.name)] = self.ax.add_artist(
            FancyArrow(x1, y1, (s1x-x1), (s1y-y1), **arrow_prop_dict)
            )
        self.artists['{}_m2'.format(self.name)] = self.ax.add_artist(
            Circle((x2,y2), radius=self.BH_scale*self.binary.m2,
                edgecolor='white',
                facecolor='black'
            )
        )
        self.artists['{}_s2'.format(self.name)] = self.ax.add_artist(
            FancyArrow(x2, y2, (s2x-x2), (s2y-y2), **arrow_prop_dict)
        )

        #cos_p12 = np.clip((s1x*s2x + s1y*s2y + s1z*s2z)/np.linalg.norm([s1x,s1y,s1z])*np.linalg.norm([s2x,s2y,s2z]),-1.,1.)
        p12 = self.binary._phi12[0]
        px = np.cos(p12)
        py = np.sin(p12)


        # make sure star that is closer is shown on top of the other, and the spin vector is on top if the spin "points out"
        if z1 > z2:
            zorder_1, zorder_2 = 5, 2
        else:
            zorder_1, zorder_2 = 2, 5
        
        if s1z > z1:
            zorder_s1 = zorder_1 + 1
        else: 
            zorder_s1 = zorder_1 - 1

        if s2z > z2:
            zorder_s2 = zorder_2 + 1
        else:
            zorder_s2 = zorder_2 - 1
        self.artists['{}_m1'.format(self.name)].set_zorder(zorder_1)
        self.artists['{}_m2'.format(self.name)].set_zorder(zorder_2)
        self.artists['{}_s1'.format(self.name)].set_zorder(zorder_s1)
        self.artists['{}_s2'.format(self.name)].set_zorder(zorder_s2)

        if self.show_phi12:
            self.artists['{}_p12'.format(self.name)] = self.ax.add_artist(
            FancyArrow(self.x00, self.y00, (px-self.x00), (py-self.y00), color='red', width=1., head_width=10.*self.BH_scale, head_length=20.*self.BH_scale)
        )

            _, dx, dy = np.matmul(self.binary.Amatrix,np.array([400.,0.,0.]))
            self.artists['{}_0axis'.format(self.name)] = self.ax.add_artist(
                FancyArrow(self.x00, self.y00, dx, dy, ls='--')
            )

            self.artists['{}_0label'.format(self.name)] = self.ax.text((self.x00+dx*1.2), -25., r"$0$", color=arrow_color, fontsize=18)

            _, dx, dy = np.matmul(self.binary.Amatrix,np.array([0.,400.,0.]))
            self.artists['{}_pib2axis'.format(self.name)] = self.ax.add_artist(
                FancyArrow(self.x00, self.y00, dx, dy, ls='--')
            )
            
            self.artists['{}_pib2label'.format(self.name)] = self.ax.text(-75.0, (self.y00+dy*1.2), r"$\pi/2$", color=arrow_color, fontsize=18)


            _, dx, dy = np.matmul(self.binary.Amatrix,np.array([-400.,0.,0.]))
            self.artists['{}_piaxis'.format(self.name)] = self.ax.add_artist(
                FancyArrow(self.x00, self.y00, dx, dy, ls='--'))

            #_, dx, dy = np.matmul(self.binary.Amatrix,np.array([0.,0.,600.]))
            #self.artists['{}_zaxis'.format(self.name)] = self.ax.add_artist(
            #    FancyArrow(self.x0, self.y0, dx, dy, **arrow_prop_dict)
            #)
            self.artists['{}_pilabel'.format(self.name)] = self.ax.text((self.x00+dx*1.3), -25., r"$\pi$", color=arrow_color, fontsize=18)

            _, dx, dy = np.matmul(self.binary.Amatrix,np.array([0.,-400.,0.]))
            self.artists['{}_3pib2axis'.format(self.name)] = self.ax.add_artist(
                FancyArrow(self.x00, self.y00, dx, dy, ls='--')
            )
            
            self.artists['{}_3pib2label'.format(self.name)] = self.ax.text(-110.0, (self.y00+dy*1.3), r"$3\pi/2$", color=arrow_color, fontsize=18)

        return self.artists

    def get_artists(self,i):
        z1,x1,y1 = self.binary.pos1_projected[i,:]
        z2,x2,y2 = self.binary.pos2_projected[i,:]
        s1z, s1x, s1y = 300.*self.binary.pos_spin1_projected[i,:]
        s2z, s2x, s2y = 300.*self.binary.pos_spin2_projected[i,:]
        self.artists['{}_m1'.format(self.name)].set_center((x1,y1))
        self.artists['{}_m2'.format(self.name)].set_center((x2,y2))
        arrow_prop_dict = dict(width=1., head_width=10.*self.BH_scale, head_length=20.*self.BH_scale)
        self.artists['{}_s1'.format(self.name)].set_data(x=x1, y=y1, dx=(s1x), dy=(s1y), **arrow_prop_dict)
        self.artists['{}_s2'.format(self.name)].set_data(x=x2, y=y2, dx=(s2x), dy=(s2y), **arrow_prop_dict) 
        if self.show_phi12:
            x00, y00 = self.x00, self.y00
            p12 = self.binary._phi12[i]
            px = 200*np.cos(p12)
            py = 200*np.sin(p12)
            self.artists['{}_p12'.format(self.name)].set_data(x=x00, y=y00, dx=(px), dy=(py), width=1., head_width=10.*self.BH_scale, head_length=20.*self.BH_scale)

        # make sure star that is closer is shown on top of the other, and the spin vector is on top if the spin "points out"
        if z1 > z2:
            zorder_1, zorder_2 = 5, 2
        elif z2 > z1:
            zorder_1, zorder_2 = 2, 5
        
        if s1z >= z1:
            zorder_s1 = zorder_1 + 1
        elif s1z < z1: 
            zorder_s1 = zorder_1 - 1

        if s2z >= z2:
            zorder_s2 = zorder_2 + 1
        elif s2z < z2:
            zorder_s2 = zorder_2 - 1
        self.artists['{}_m1'.format(self.name)].set_zorder(zorder_1)
        self.artists['{}_m2'.format(self.name)].set_zorder(zorder_2)
        self.artists['{}_s1'.format(self.name)].set_zorder(zorder_s1)
        self.artists['{}_s2'.format(self.name)].set_zorder(zorder_s2)

        return self.artists