from importlib.metadata import PackageNotFoundError

try:
    from ._version import version as __version__
except PackageNotFoundError:
    # package not installed
    pass
