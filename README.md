# Twirl
### A tool for building cartoon animations of binaries.
### Warning: This tool is currently under major development

## Compatibility / Dependencies
This code is only supported in Python 3. It also depends on 
Matplotlib and Numpy.

## Installation
Clone this repository and install from source:
```
git clone https://git.ligo.org/zoheyr-doctor/twirl.git
cd twirl
pip install .
```

## Tools
The main tool currently (this code is still under major development) is 
the module `twirl.binary`.  This module contains the `Binary` class that
defines individual binaries by their intrinsic parameters (masses, spins...)
and extrinsic parameters (viewing angle, as specified by Euler angles). The
binary can then be evolved in its rotation with `Binary.orbit()` and projected
into the viewer's coordinates with `Binary.project()`. The child class 
`InspiralingBinary` will perform an inspiral on the binary based on 
gravitational wave emission.

To facilitate animating the binaries, the module `twirl.artists` defines
classes for different objects you may want to animate.  Currently, there are
the `BinaryBlackHole` and `BlackHoleRingdown` objects that will automatically
generate the Matplotlib artists for animation.

A preliminary script that uses these tools is `animate_binaries_from_file`, which
defines a binary for each event in a catalog and animates each binary in its
own subplot.  It creates an animation like this one:

![](examples/orbit.mp4)

An inspiral and ringdown video is shown here:

![](examples/inspiral_ringdown.mp4)
