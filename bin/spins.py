#!/usr/bin/env python

import numpy as np
from twirl.artists import BlackHoleRingdown, BinaryBlackHole, BBH_with_spins
from twirl.binary import InspiralingBinary, Binary_with_spins, Spin_Projection
from matplotlib import pyplot as plt
from matplotlib import animation

#plt.style.use('dark_background') 
fig,ax = plt.subplots(figsize=(20,20))
ax.set_axis_off()

binary = Binary_with_spins(
    m1=30.,
    m2=25.,
    alpha=90.,
    beta=0.,
    gamma=180., 
    frame_rate=30.,
    omega0 = 8.,
    chi1 = 0.95,
    chi2 = 0.95,
    tilt1 = np.pi/2,
    tilt2 = np.pi/2.1,
    phi12 = 0
)
binary.inspiral()

G = 4.3e-3 * 3.1e13  # km Msol^-1 (km/s)^2
c = 3e5

extent = 5000

bbh = BBH_with_spins(
    binary=binary,
    ax = ax,
    name='BBH',
    BH_scale=8.,
    show_axes=True
) 



ringdown = BlackHoleRingdown(
    M=60.,
    amplitude0 = 1.,
    omega=0.03,
    tau=15,
    ax=ax,
    name='BH',
    BH_scale = 12.
)

def init():
    ax.set_xlim([-extent,extent])
    ax.set_ylim([-extent,extent])
    return bbh.setup_artists()

def animate(i):
    if i == binary.nframes: 
        for name,artist in bbh.artists.items():
            artist.remove()     
        ringdown.setup_artists()
    if i > binary.nframes:
        return ringdown.get_artists(i-binary.nframes)
    if i < binary.nframes:
        return bbh.get_artists(i)

fps = 30
anim = animation.FuncAnimation(fig,animate,init_func=init,frames = binary.nframes+60)
anim.save('inspiral_sideview2.mp4',fps=fps,extra_args=['-vcodec', 'libx264'])
 

