#!/usr/bin/env python
# Create top-down projection visualization to track spin morphologies
import numpy as np
from twirl.artists import BlackHoleRingdown, BinaryBlackHole, BBH_with_spins, BBH_spin_projection
from twirl.binary import InspiralingBinary, Binary_with_spins, Spin_Projection
from matplotlib import pyplot as plt
from matplotlib import animation

#plt.style.use('dark_background') 
fig,ax = plt.subplots(figsize=(12,12))
ax.set_axis_off()

binary = Spin_Projection(
    m1=40.0,
    m2=35.0,
    projection='XY',
    frame_rate=30.,
    omega0 = 2.5,
    chi1 = 0.95,
    chi2 = 0.95,
    tilt1 = 0.8,
    tilt2 = 2.3,
    phi12 = 0.9
)
binary.inspiral()


G = 4.3e-3 * 3.1e13  # km Msol^-1 (km/s)^2
c = 3e5

extent = 2000

bbh = BBH_spin_projection(
    binary=binary,
    ax = ax,
    name='BBH',
    BH_scale=3.2,
    show_phi12=True
) 



ringdown = BlackHoleRingdown(
    M=60.,
    amplitude0 = 1.,
    omega=0.03,
    tau=15,
    ax=ax,
    name='BH',
    BH_scale = 12.
)

def init():
    ax.set_xlim([-extent,extent])
    ax.set_ylim([-extent,extent])
    return bbh.setup_artists()

def animate(i):
    return bbh.get_artists(i)

fps = 30
anim = animation.FuncAnimation(fig,animate,init_func=init,frames = binary.nframes)
anim.save('inspiral_topview.mp4',fps=fps,extra_args=['-vcodec', 'libx264'])
 
### EXAMPLES:
## L\pi morphology:
# m1=40.90909090909091, m2=34.090909090909086, (chi1,chi2)=(0.95,0.95), tilt1=1.0, tilt2=0.5, phi12=3.2 

## L0 morphology:
# m1=40.90909090909091, m2=34.090909090909086, (chi1,chi2)=(0.95,0.95), tilt1=0.8, tilt2=2.3, phi12=0.9

## C morphology:
# m1=40.90909090909091, m2=34.090909090909086, (chi1,chi2)=(0.95,0.95), tilt1=1.3, tilt2=2.5, phi12=3.0


