./animate_binaries_from_file confidence_intervals_for_release.json --outfile o3b_final_resubmit --round --dark --nxny 6 6 --figsize 12 9 --credit "Zoheyr Doctor | CIERA | LVK" --name_file names.json

./animate_binaries_from_file confidence_intervals_for_release.json --outfile o3b_final_resubmit_no_names --round --dark --nxny 6 6 --credit "Zoheyr Doctor | CIERA | LVK" --no_names

